
module MyFraction where
import Test.QuickCheck

type Fraction = (Integer, Integer)

--formalize to make sign on fst
forma :: Fraction -> Fraction
forma (a, b) 
   | b < 0 = (-1 * a, -1 * b)
   |otherwise = (a, b)

divgcd :: Fraction -> Fraction
-- div 0 quiz
divgcd (_, 0) = (0, 0)
divgcd (a, b) = forma (div a  g, div  b g) where g = gcd a b

ratplus :: Fraction -> Fraction -> Fraction
ratplus (x1, y1) (x2, y2) = divgcd (x1 * y2 + x2 * y1, y1 * y2)

ratminus :: Fraction -> Fraction -> Fraction
ratminus (x1, y1) (x2, y2) = divgcd (x1 * y2 - x2 * y1, y1 * y2)

rattimes :: Fraction -> Fraction -> Fraction
rattimes (x1, y1) (x2, y2) = divgcd (x1 * x2, y1 * y2)

ratdiv :: Fraction -> Fraction -> Fraction
ratdiv (x1, y1) (x2, y2) = rattimes (x1, y1) (y2, x2)

ratfloor :: Fraction -> Integer
ratfloor (x1, y1) = div x1 y1

ratfloat :: Fraction -> Float
ratfloat (x1, y1) = fromInteger x1 / fromInteger y1

(<+>) :: Fraction -> Fraction -> Fraction
infixl 6 <+>
a <+> b = ratplus a b

(<->) :: Fraction -> Fraction -> Fraction
infixl 6 <->
a <-> b = ratminus a b

(<*>) :: Fraction -> Fraction -> Fraction
infixl 7 <*>
a <*> b = rattimes a b

(</>) :: Fraction -> Fraction -> Fraction
infixl 7 </>
a </> b = ratdiv a b

(<==>) :: Fraction -> Fraction -> Bool
infixl 4 <==>
(x1, y1) <==> (x2, y2)
     | x1 == x2 && y1 == y2 = True
     | otherwise = False


squareroot2 :: Float -> Integer -> Float
squareroot2 x 0 = x
squareroot2 x 1= (x + 2 / x ) / 2
squareroot2 x n =  squareroot2 (squareroot2 x 1) (n-1)

squareroot :: Float -> Float -> Integer -> Float
squareroot nn x 0 = nn
squareroot nn x 1 = (x + nn / x) / 2
squareroot nn x n = squareroot nn (squareroot nn x 1) (n-1)

sqrtSeq :: Float -> Float -> [Float]
sqrtSeq n x = map (squareroot n x) [1..]

within :: Float -> [Float] -> Float
within eps (a:(b:rest))
       | abs(a - b) < eps = b
       | otherwise = within eps (b : rest)
squareroot' :: Float -> Float -> Float -> Float
squareroot'  n x epsilon = within epsilon (sqrtSeq n x)


epsilon = 1e-5
eq a b = 1e-5 > (a -b)
prop_ratplus a b =  (snd a == 0 || snd b == 0) || (
                ((a <+> b) <+> b) == (a <+> (b <+> b)) 
             && (a <+> b) <+> (0, 1) == (a <+> b)
             && eq (ratfloat (a<+>b))  (ratfloat a + ratfloat b)
               )

prop_ratminus a b =  (snd a == 0 || snd b == 0) || (
                ((a <-> b) <-> b) == (a <-> (b <+> b)) 
             && (a <-> b) <-> (0, 1) == (a <-> b)
             && eq (ratfloat (a<->b))  (ratfloat a - ratfloat b)
               )

prop_rattimes a b =  (snd a == 0 || snd b == 0) || (
                ((a <*> b) <*> b) == (a <*> (b <*> b)) 
             && (a <*> b) <*> (1, 1) == (a <*> b)
             && eq (ratfloat (a<*>b))  (ratfloat a * ratfloat b)
               )
prop_ratdiv a b =  (snd a == 0 || snd b == 0 || fst b == 0) || (
                ((a </> b) </> b) == (a </> (b <*> b)) 
             && (a </> b) </> (1, 1) == (a </> b)
             && eq (ratfloat (a</>b))  (ratfloat a / ratfloat b)
               )

prop_root n = (n < 0) || eq (squareroot' n 1 1e-6) (sqrt n)
