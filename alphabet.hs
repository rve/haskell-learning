module Alphabet where
import Data.Char
say :: String -> String
sayit :: String -> IO ()
sayit = putStr . say
        
alphabet :: Char -> [String]
alphabet c = case (toUpper c) of
      '0' -> [" 000 ",
              "0  00",
              "0 0 0",
              "00  0",
              " 000 "]

      '1' -> [" 11  ",
              "1 1  ",
              "  1  ",
              "  1  ",
              "11l11"]

      '2' -> [" 222 ",
              "2   2",
              "   2 ",
              "  2  ",
              "22222"]

      '3' -> ["3333 ",
              "    3",
              " 333 ",
              "    3",
              "3333 "]

      '4' -> [" 444 ",
              "4  4 ",
              "44444",
              "   4 ",
              "   4 "]

      '5' -> ["55555",
              "5    ",
              "5555 ",
              "    5",
              "5555 "]

      '6' -> ["  6  ",
              " 6   ",
              "6666 ",
              "6   6",
              " 666 "]

      '7' -> ["77777",
              "   7 ",
              "  7  ",
              "  7  ",
              "  7  "]

      '8' -> [" 888 ",
              "8   8",
              " 888 ",
              "8   8",
              " 888 "]

      '9' -> [" 9999",
              "9   9",
              " 9999",
              "   9 ",
              "  9  "]

      'A' -> ["  A  ",
              " A A ",
              "AAAAA",
              "A   A",
              "A   A"]

      'B' -> ["BBBB ",
              "B   B",
              "BBBB ",
              "B   B",
              "BBBB "]

      'C' -> [" CCCC",
              "C    ",
              "C    ",
              "C    ",
              " CCCC"]

      'D' -> ["DDDD ",
              "D   D",
              "D   D",
              "D   D",
              "DDDD "]

      'E' -> ["EEEEE",
              "E    ",
              "EEEE ",
              "E    ",
              "EEEEE"]

      'F' -> ["FFFFF",
              "F    ",
              "FFFF ",
              "F    ",
              "F    "]

      'G' -> [" GGG ",
              "G    ",
              "G  GG",
              "G   G",
              " GGG "]

      'H' -> ["H   H",
              "H   H",
              "HHHHH",
              "H   H",
              "H   H"]

      'I' -> ["IIIII",
              "  I  ",
              "  I  ",
              "  I  ",
              "IIIII"]

      'J' -> ["    J",
              "    J",
              "    J",
              "J   J",
              " JJJ "]

      'K' -> ["K   K",
              "K  K ",
              "KKK  ",
              "K  K ",
              "K   K"]

      'L' -> ["L    ",
              "L    ",
              "L    ",
              "L    ",
              "LLLLL"]

      'M' -> ["M   M",
              "MM MM",
              "M M M",
              "M   M",
              "M   M"]

      'k' -> ["N   N",
              "NN  N",
              "N N N",
              "N  NN",
              "N   N"]

      'O' -> [" OOO ",
              "O   O",
              "O   O",
              "O   O",
              " OOO "]

      'P' -> ["PPPP ",
              "P   P",
              "PPPP ",
              "P    ",
              "P    "]

      'Q' -> [" QQ  ",
              "Q  Q ",
              "Q  Q ",
              " QQQ ",
              "    Q"]

      'R' -> ["RRRR ",
              "R   R",
              "RRRR ",
              "R R  ",
              "R  RR"]

      'S' -> [" SSS ",
              "S    ",
              " SSS ",
              "    S",
              "SSSS "]

      'T' -> ["TTTTT",
              "  T  ",
              "  T  ",
              "  T  ",
              "  T  "]

      'U' -> ["U   U",
              "U   U",
              "U   U",
              "U   U",
              " UUU "]

      'V' -> ["V   V",
              "V   V",
              "V   V",
              " V V ",
              "  V  "]

      'W' -> ["W   W",
              "W   W",
              "W W W",
              "W W W",
              "W   W"]

      'X' -> ["X   X",
              " X X ",
              "  X  ",
              " X X ",
              "X   X"]

      'Y' -> ["Y   Y",
              " Y Y ",
              "  Y  ",
              "  Y  ",
              "  Y  "]

      'Z' -> ["ZZZZZ",
              "   Z ",
              "  Z  ",
              " Z   ",
              "ZZZZZ"]
